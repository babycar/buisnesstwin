# -*- coding: utf-8 -*-
"""
Created on Tue May 14 12:51:28 2019

@author: Zoya
"""

class Business():
    def __init__(self,params, income = 10,profit = 100, cost = 100000, menu = []):
        self.income = income
        self.profit = profit
        self.cost = cost
        self.params = params
        self.month_income = 0
        self.menu = menu
        
    def __str__(self):
        return """Income = %s | Profit = %s
               """%(self.income, self.profit)
    
    def reculc(self):
        self.profit += self.month_income - self.cost
       # print(self.month_income - self.cost)
        self.income += self.month_income
        print(self.month_income)
        
    def getTypesFromMenu(menu, key):
        types = []
        for menuItem in menu:
            if not (menuItem[key] in types):
                types.append(menuItem[key])
        return types
    
    def getSolvencyFromMenu(menu):
        solvency = {'min': 100000, 'max': 0, 'mean': 0}
        priceSum = 0
        
        for menuItem in menu:
            priceSum += menuItem['price']
            if menuItem['price'] > solvency['max']:
                solvency['max'] = menuItem['price']
            if menuItem['price'] < solvency['min']:
                solvency['min'] = menuItem['price']
                
        solvency['mean'] = priceSum / len(menu)
        
        return solvency
        
        
    