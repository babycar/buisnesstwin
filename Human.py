# -*- coding: utf-8 -*-
"""
Created on Tue May 14 12:42:54 2019

@author: 

"""
class Human():
    st_params = {}
    def __init__(self):
        self.params = {}
       # self.payment = 
        pass
    
    def __str__(self):
        out = ""
        for key in self.st_params:
            out += str(self.params[key])+"\n"
        return out
        
    
    def set_params(self):
        for key in self.st_params:
            self.params[key] = self.st_params[key].copy().generate()
           # print(self.params[key] )
        
        return self
    
    def pay(self, menu):
        payment = 0
        for menuItem in menu:
            if (payment >= self.params["Solvency"].params["max"]):
                break
            if (menuItem["dish_type"] in self.params["DishType"].params["dish_type"]) and (menuItem["dish_flavor"] in self.params["DishFlavor"].params["dish_flavor"]) :
                payment += menuItem["price"]
#        print(payment)
            
        return payment
    
