# -*- coding: utf-8 -*-
"""
Created on Tue May 14 12:13:31 2019

@author: Zoya
"""
import random

class Parametr(object):    
    def __init__(self, params = None, default = None):
        self.default = default if default!=None else {}
        self.params = params if params!=None else {}     
        
    def __str__(self):
        p = ''
        for key in self.params.keys():
            p+=key+":"
            p+=str(self.params[key])+"\n"
        return p
    
    def calculate(self):
        pass
    
    def clear(self):
        for key in self.params.keys():
            self.params[key] = None
        return self
    
    def generate(self):
        for key in self.default.keys():
            self.params[key] = random.choice(self.default[key])
        return self
    
    def copy(self):
        pass
    

class Remoteness(Parametr): #Удаленность   
    default_x = list(range(0,1000))
    default_y = list(range(0,1000))
    def __init__(self, params = None, default = None):
        self.default = default if default!=None else {'x': self.default_x, 'y':self.default_y}
        super().__init__(params, self.default)
        
    def calculate(self, p):

        v = ((p.params['x'] - self.params['x'])**2 + (p.params['y'] - self.params['y'])**2)**0.5 #вычисляем в метрах

        '''if  1000 < v < 4000:
            return 10
        if 20 < v < 1000:
            return 50
        if v < 20:
            return 100
        return 0'''

        if v >= 5000:
            probability = 0
        else:
            probability = 100 - v/50
        
        return probability 
    
    def copy(self):
        return Remoteness()



class Quality(Parametr):  # качество
	default_quality = ['низкое', 'среднее', 'высокое']

	def __init__(self, params=None, default=None):
		self.default = default if default != None else {'quality': self.default_quality}
		super().__init__(params, self.default)

	def calculate(self, human):
		translate = {"низкое": 0, "среднее": 1, "высокое": 2}
		influence = [
			[0, 20, 60],
			[20, 60, 80],
			[60, 80, 100]
		]
		hp = translate[human.params['quality']]
		bp = translate[self.params['quality']]

		return influence[bp][hp]

	def copy(self):
		return Quality()


class AdvertisingForce(Parametr):  # реклама
	default_force = ['слабый', 'средний', 'сильный']

	def __init__(self, params=None, default=None):
		self.default = default if default != None else {'force': self.default_force}
		super().__init__(params, self.default)

	def calculate(self, human):
		translate = {"слабый": 0, "средний": 1, "сильный": 2}
		influence = [
			[0, 20, 60],
			[20, 60, 80],
			[60, 80, 100]
		]
		hp = translate[human.params['force']]
		bp = translate[self.params['force']]

		return influence[bp][hp]

	def copy(self):
		return AdvertisingForce()
        
    
class Solvency(Parametr):
    default_solvency_max = 500
    default_solvency_min = 75
    
    def __init__(self, params = None, default = None):
        self.default = default if default!= None else {'max':self.default_solvency_max,
                                                       'min':self.default_solvency_min}
        super().__init__(params, self.default)
    
    #self Бизнес
    #human Человек 
    def calculate(self, human): # Сколько человек готов потратить
        maxx = human.params['max']
        
        if maxx - self.params['min'] < 0:
            return 0
        if maxx - self.params['max']  >= 0:
            return 100
        
        Step = (self.params['max'] - self.params['min'])/90
        Coefficient = self.params['max'] - human.params['max']
        Coefficient = 90 - Coefficient/Step
        return Coefficient
    
    def generate(self):
        self.params['min'] = random.randint(self.default['min'], self.default['max'])
        self.params['max'] = random.randint(self.params['min'], self.default['max'])
        self.params['mean'] = random.randint(self.params['min'], self.params['max'])
        
        return self
        
    def copy(self):
        return Solvency()
    
    
class DishType(Parametr):
    default_dish_type = ["блюда гриль", "закуски", "супы", "горячие блюда", "салаты", "напитки", "десерты", "пиццы", "пасты", "суши", "морепродукты", "алкоголь", "выпечка"]

    def __init__(self, params = None, default = None):
        self.default = default if default != None else {'dish_type': self.default_dish_type}
        super().__init__(params, self.default)
        
    def calculate(self, human):
        humanParam = human.params["dish_type"]
        businessParam = self.params["dish_type"]
        
        coincidences = 0
        for param in humanParam:
            if param in businessParam:
                coincidences += 1
        
        return coincidences / len(humanParam) * 100
    
    def generate(self):
        for key in self.default.keys():
            self.params[key] = []
            for i in range(random.randint(1, 3)):
                self.params[key].append(random.choice(self.default[key]))
        return self
    
    def copy(self):
        return DishType()
    
    
class DishFlavor(Parametr):
    default_dish_flavor = ["сладкое", "нейтральное", "острое", "кислое", "горькое", "освежающее", "крепкое"]

    def __init__(self, params = None, default = None):
        self.default = default if default != None else {'dish_flavor': self.default_dish_flavor}
        super().__init__(params, self.default)
        
    def calculate(self, human):
        humanParam = human.params["dish_flavor"]
        businessParam = self.params["dish_flavor"]
        
        coincidences = 0
        
        for param in humanParam:
            if param in businessParam:
                coincidences += 1
        
        return coincidences / len(humanParam) * 100
    
    def generate(self):
        for key in self.default.keys():
            self.params[key] = []
            for i in range(random.randint(3, 5)):
                self.params[key].append(random.choice(self.default[key]))
        return self
    
    def copy(self):
        return DishFlavor()
        
    
    
if __name__ == "__main__":
 
    R_human = Remoteness()
    R_human.generate()
    R_buisness = Remoteness({'x':2,'y':10},{'x':[100,4,5,450,3000],'y':[100,4,5,100,3000]})
    R_buisness.generate()
    
    print(R_human)
    print("Посчитали благоприятность расстояния: ", R_buisness.calculate(R_human))
    
    A_human = AdvertisingForce()
    A_human.generate()
    A_buisness = AdvertisingForce({'force':"сильный"})     
   
    print("Посчитали благоприятность силы рекламы: ",A_buisness.calculate(A_human))
    
    S_human = Solvency()
    S_human.generate()
    S_buisness = Solvency({'mean':300, 'max':3000, 'min':450})     
   
    print("Посчитали благоприятность по платежеспособности: ",S_buisness.calculate(S_human))
        