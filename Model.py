# -*- coding: utf-8 -*-
"""
Created on Tue May 14 12:09:15 2019

@author: Zoya
"""

from Human import Human
from Parametr import Remoteness, Quality, AdvertisingForce, DishType, DishFlavor, Solvency
from Business import Business

class Model:    
    def __init__(self, params, menu):
        self.business = Business(params, income = 10, profit = 100,
                                 cost = 100000, menu = menu)
        self.cur_population = []
        Human.st_params = params

    def epoch(self):
        self.generate_population() 
        
        visitors = 0
        visitorsPay = 0
        for human in self.cur_population:
            sum_params = 0 
            for key in self.business.params:
#                print(key + ": " + str(self.business.params[key].calculate(human.params[key])))
                sum_params += self.business.params[key].calculate(human.params[key])
              
            avg_params = sum_params / len(self.business.params.keys())
            if avg_params > 60:
                self.business.month_income += human.pay(self.business.menu)
                visitorsPay += human.pay(self.business.menu)
                visitors += 1
        print(str(visitors / len(self.cur_population) * 100) + "% people visited, paid " + str(visitorsPay))
        self.business.reculc() 
        self.business.month_income = 0
        return 
            
    def predict(self, n):
        for _ in range(n):
            self.epoch()
        return self.business
    
    def generate_population(self, count = 1000):
        
        self.cur_population = []
        for i in range(count):
            h = Human()
            h.set_params()
           # print(i," - \n",h)
            self.cur_population.append(h)
            
#        for i in range(count):
#            print(self.cur_population[i])
        
        return self
    

if __name__ == "__main__":
    menu = [{"dish_type": "блюда гриль", "dish_flavor": "острое", "price": 495}, {"dish_type": "салаты", "dish_flavor": "освежающее", "price": 290}, {"dish_type": "закуски", "dish_flavor": "кислое", "price": 400}, 
            {"dish_type": "супы", "dish_flavor": "нейтральное", "price": 279}, {"dish_type": "пиццы", "dish_flavor": "нейтральное",  "price": 275}, {"dish_type": "пасты", "dish_flavor": "острое", "price": 299}, 
            {"dish_type": "горячие блюда", "dish_flavor": "нейтральное", "price": 430},{"dish_type": "десерты", "dish_flavor": "сладкое", "price": 285}, {"dish_type": "напитки", "dish_flavor": "крепкое", "price": 210}]
    
    R_business = Remoteness({'x':2,'y':10},
                             {'x':[100,4,5,450,3000],'y':[100,4,5,100,3000]})
    Q_business = Quality({'quality': "среднее"}) 
    A_business = AdvertisingForce({'force':"слабый"}) 
    S_business = Solvency(Business.getSolvencyFromMenu(menu))   
    DT_business = DishType({'dish_type': Business.getTypesFromMenu(menu, 'dish_type')})
    DF_business = DishFlavor({'dish_flavor': Business.getTypesFromMenu(menu, 'dish_flavor')})
    
    params = {"Remoteness": R_business, "Quality": Q_business, "AdvertisingForce": A_business, "Solvency": S_business, "DishType": DT_business, "DishFlavor": DF_business}
     
    model = Model(params, menu)
    predicted = model.predict(12)
    print(predicted)